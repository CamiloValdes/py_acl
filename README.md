# Caja Los Héroes

Prueba Técnica Caja Los Héroes

* Desarrollo enviado solo "src", archivo "build tool" y archivo "pom.xml" de manera que para revisar, se debe descargar las dependencias (MAVEN) y ejecutar el run.
* La Base de Datos es embebido (DB2) por ende, no necesita configuración (auto-ejecutable)

# Accesos

Swagger
- /v2/api-docs
- /swagger-ui/

H2
- /h2-console/login.jsp

Fetch_Token
@RequestParam
	- user: usuario
	- password: pwd

- /auth
