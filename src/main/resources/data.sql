DROP TABLE IF EXISTS persona;

CREATE TABLE persona (
  id bigint AUTO_INCREMENT PRIMARY KEY,
  national_id varchar(250) NOT NULL,
  first_name varchar(250) NULL,
  last_name varchar(250) NULL,
  birthdate date NULL,
  has_insurance boolean NULL,
  phone varchar(50) NULL,
  email varchar(250) NULL,
  created_at date NULL,
  modified_at date NULL
);

INSERT INTO persona (national_id, first_name, last_name, birthdate, has_insurance, phone, email) VALUES
  ('11111111-1', 'Aliko', 'Dangote', CURRENT_TIMESTAMP, 1, '9876543210', 'correo1@correo.cl'),
  ('22222222-2', 'Bill', 'Gates', CURRENT_TIMESTAMP, 1, '9876543210', 'correo2@correo.cl'),
  ('33333333-3', 'Folrunsho', 'Alakija', CURRENT_TIMESTAMP, 0, '9876543210', 'correo3@correo.cl'),
  ('44444444-4', 'Bill', 'Dangote', CURRENT_TIMESTAMP, 1, '9876543210', 'correo4@correo.cl'),
  ('55555555-5', 'Aliko', 'Gates', CURRENT_TIMESTAMP, 0, '9876543210', 'correo5@correo.cl');
