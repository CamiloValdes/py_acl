package cl.acl.mserv.service;

import java.util.List;

import cl.acl.mserv.dto.ReqPersonaDto;
import cl.acl.mserv.dto.ResPersonaDto;

public interface PersonaService {

	ResPersonaDto create(ReqPersonaDto req);

	ResPersonaDto update(Long id, ReqPersonaDto req);

	ResPersonaDto delete(Long id);

	List<ReqPersonaDto> fetchAllPersona();

	ReqPersonaDto fetchPersona(Long id);

}
