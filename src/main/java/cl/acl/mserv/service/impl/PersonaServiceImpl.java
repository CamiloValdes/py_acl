package cl.acl.mserv.service.impl;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Service;

import cl.acl.mserv.dto.ReqPersonaDto;
import cl.acl.mserv.dto.ResPersonaDto;
import cl.acl.mserv.entity.PersonaEntity;
import cl.acl.mserv.exception.TechnicalException;
import cl.acl.mserv.repository.PersonaRepository;
import cl.acl.mserv.service.PersonaService;
import cl.acl.mserv.utils.UtilsText;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PersonaServiceImpl implements PersonaService {

	private final PersonaRepository personaRepository;
	private final UtilsText utilsText;
	private final ModelMapper mapper;

	@Autowired
	public PersonaServiceImpl(PersonaRepository personaRepository, UtilsText utilsText, ModelMapper mapper) {
		this.personaRepository = personaRepository;
		this.utilsText = utilsText;
		this.mapper = mapper;
	}

	@Override
	public ResPersonaDto create(ReqPersonaDto req) {

		if (Boolean.FALSE.equals(this.validateEmail(req.getEmail()))) {
			String error = String.format("Email is duplicate %s", req.getEmail());
			log.error(error);
			throw new TechnicalException(error);
		}

		if (Boolean.FALSE.equals(this.validateNationalId(req.getNationalId()))) {
			String error = String.format("NationalId is duplicate %s", req.getEmail());
			log.error(error);
			throw new TechnicalException(error);
		}

		if (null != req.getBirthdate()) {
			req.setBirthdate(
					this.utilsText.formatDateString(String.format("%sT00:00:00.000-04:00", req.getBirthdate())));
		}

		PersonaEntity createdPersonaEntity = this.personaRepository
				.save(this.convertToEntity(req, new PersonaEntity()));

		return ResPersonaDto.builder().id(createdPersonaEntity.getId()).comment("Nueva Persona creada correctamente")
				.build();
	}

	@Override
	public ResPersonaDto update(Long id, ReqPersonaDto req) {
		Optional<PersonaEntity> optionalExistingPersona = this.personaRepository.findById(id);

		if (optionalExistingPersona.isPresent()) {
			PersonaEntity personaEntity = optionalExistingPersona.get();

			if (Boolean.FALSE.equals(this.validateEmail(req.getEmail()))
					&& !personaEntity.getEmail().equals(req.getEmail())) {
				String error = String.format("Email is duplicate %s", req.getEmail());
				log.error(error);
				throw new TechnicalException(error);
			}

			if (Boolean.FALSE.equals(this.validateNationalId(req.getNationalId()))
					&& !personaEntity.getNationalId().equals(req.getNationalId())) {
				String error = String.format("NationalId is duplicate %s", req.getEmail());
				log.error(error);
				throw new TechnicalException(error);
			}

			PersonaEntity toUpdatePersonaEntity = this.convertToEntity(req, personaEntity);
			PersonaEntity updatedPersonaEntity = this.personaRepository.save(toUpdatePersonaEntity);

			return ResPersonaDto.builder().id(updatedPersonaEntity.getId()).comment("Persona actualizada correctamente")
					.build();

		} else {
			log.error("Persona record not found ID: {}", id);
			throw new NoSuchElementException("Persona record not found ID:" + id);
		}
	}

	@Override
	public ResPersonaDto delete(Long id) {
		Optional<PersonaEntity> optPersona = this.personaRepository.findById(id);
		if (optPersona.isPresent()) {
			this.personaRepository.delete(optPersona.get());
			return ResPersonaDto.builder().id(id).comment("Persona eliminada correctamente").build();
		}
		return ResPersonaDto.builder().id(id).comment("Persona no encontrada").build();
	}

	@Override
	public List<ReqPersonaDto> fetchAllPersona() {
		List<PersonaEntity> lst = this.personaRepository.findAll();
		return lst.stream().map(p -> {
			return new ReqPersonaDto(p.getId(), p.getNationalId(), p.getFirstName(), p.getLastName(), p.getBirthdate(),
					p.getHasInsurance(), p.getPhone(), p.getEmail());
		}).collect(Collectors.toList());
	}

	@Override
	public ReqPersonaDto fetchPersona(Long id) {
		Optional<PersonaEntity> optionalExistingPersona = this.personaRepository.findById(id);
		if (optionalExistingPersona.isPresent()) {
			PersonaEntity p = optionalExistingPersona.get();
			return new ReqPersonaDto(p.getId(), p.getNationalId(), p.getFirstName(), p.getLastName(), p.getBirthdate(),
					p.getHasInsurance(), p.getPhone(), p.getEmail());
		} else {
			String error = String.format("Persona record not found ID: %s", id);
			log.error(error);
			throw new TechnicalException(error);
		}
	}

	// Commons

	private PersonaEntity convertToEntity(ReqPersonaDto req, PersonaEntity existingPersonaEntity)
			throws ParseException {
		this.mapper.getConfiguration().setSkipNullEnabled(true);
		this.mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		this.mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
		this.mapper.map(req, existingPersonaEntity);
		this.handleCreatedAndModifiedDateTime(existingPersonaEntity);
		return existingPersonaEntity;
	}

	private void handleCreatedAndModifiedDateTime(PersonaEntity personaEntity) {
		if (personaEntity.getId() == null) {
			personaEntity.setCreatedAt(new Date());
		} else {
			personaEntity.setModifiedAt(new Date());
		}
	}

	private Boolean validateEmail(String email) {
		return this.personaRepository.findByEmail(email).isPresent() ? Boolean.FALSE : Boolean.TRUE;
	}

	private Boolean validateNationalId(String nationalId) {
		return this.personaRepository.findByNationalId(nationalId).isPresent() ? Boolean.FALSE : Boolean.TRUE;
	}

}