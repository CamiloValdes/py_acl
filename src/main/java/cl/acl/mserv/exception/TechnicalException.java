package cl.acl.mserv.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TechnicalException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private final String trace;

	public TechnicalException(String trace) {
		this.trace = trace;
	}
}
