package cl.acl.mserv.dto;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Email;

import cl.acl.validator.annotation.Rut;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Valid
public class ReqPersonaDto {
	private Long id;

	@Rut
	private String nationalId;

	private String firstName;

	private String lastName;

	private Date birthdate;

	private Boolean hasInsurance;

	private String phone;

	@Email(message = "Email should be valid. Ex: john.doe@mail.com")
	private String email;
}