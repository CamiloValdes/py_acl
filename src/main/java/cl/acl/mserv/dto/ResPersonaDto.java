package cl.acl.mserv.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@AllArgsConstructor
@Data
@Builder
@ToString
public class ResPersonaDto {
	private Long id;
	private String comment;
}
