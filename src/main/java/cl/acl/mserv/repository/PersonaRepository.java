package cl.acl.mserv.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.acl.mserv.entity.PersonaEntity;

@Repository
public interface PersonaRepository extends JpaRepository<PersonaEntity, Long> {

	Optional<PersonaEntity> findByEmail(String email);

	Optional<PersonaEntity> findByNationalId(String nationalId);

}