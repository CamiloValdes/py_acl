package cl.acl.mserv.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cl.acl.mserv.dto.ReqPersonaDto;
import cl.acl.mserv.dto.ResPersonaDto;
import cl.acl.mserv.service.PersonaService;
import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@EnableAutoConfiguration
@RequestMapping("/v1/acl/persona")
public class PersonaController {

	private final PersonaService personaService;

	@Autowired
	public PersonaController(PersonaService personaService) {
		this.personaService = personaService;
	}

	@PostMapping(value = "/")
	@ApiOperation("Create new Persona")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ResPersonaDto createPersona(@Valid @RequestBody ReqPersonaDto req) {
		return this.personaService.create(req);
	}

	@PutMapping(value = "/{id}")
	@ApiOperation("Update Persona ById")
	@ResponseStatus(code = HttpStatus.OK)
	public ResPersonaDto updatePersona(@PathVariable("id") Long id, @Valid @RequestBody ReqPersonaDto req) {
		return this.personaService.update(id, req);
	}

	@DeleteMapping(value = "/{id}")
	@ApiOperation("Delete Persona ById")
	@ResponseStatus(code = HttpStatus.OK)
	public ResPersonaDto removePersona(@PathVariable("id") Long id) {
		return this.personaService.delete(id);
	}

	@GetMapping(value = "/all")
	@ApiOperation("Fetch AllPersona")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<List<ReqPersonaDto>> fetchAll() {
		return new ResponseEntity<>(this.personaService.fetchAllPersona(), HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	@ApiOperation("Fetch Persona ById")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<ReqPersonaDto> fetchId(@PathVariable("id") Long id) {
		return new ResponseEntity<>(this.personaService.fetchPersona(id), HttpStatus.OK);
	}

}