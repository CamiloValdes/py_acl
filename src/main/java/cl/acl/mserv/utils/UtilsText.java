package cl.acl.mserv.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class UtilsText {

	private static final String		FORMAT_DATE_OUT				=	"yyyy-MM-dd";
	private static final String		FORMAT_DATE_IN 				=	"dd-MM-yyyy";

	/*************************************************************************************************************************************/

	public String formatDate(Date date) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE_OUT);
			return sdf.format(date);
		}catch(Exception ex) {
			return date.toString();
		}
	}
	
	public Date formatDateString(String date) {
		try {
			return new SimpleDateFormat(FORMAT_DATE_IN).parse(date);
		}catch(Exception ex) {
			return null;
		}
	}

}