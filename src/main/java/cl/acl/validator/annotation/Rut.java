package cl.acl.validator.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import cl.acl.validator.RutValidator;

@Constraint(validatedBy = RutValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Rut {

	String message() default "RUT field incorrect; should be valid and with format e.g. 11111111-1";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
