package cl.acl.validator;

import static org.apache.logging.log4j.util.Strings.isBlank;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import cl.acl.validator.annotation.Rut;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RutValidator implements ConstraintValidator<Rut, String> {

	@Override
	public void initialize(Rut constraintAnnotation) {
		// Do nothing
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
		try {
			// if rut is present, validate that it hasn't any dot
			if (!isBlank(value) && value.contains(".")) {
				return false;
			}

			if (isBlank(value)) {
				return true;
			}

			return validate(value);
		} catch (Exception e) {
			log.warn("isValid | message={}", e.getMessage());
			return false;
		}
	}

	public boolean validate(String rutDv) {
		rutDv = clean(rutDv);
		int rut = getRut(rutDv);
		char dv = getDv(rutDv);
		int m = 0;
		int s = 1;
		for (; rut != 0; rut /= 10) {
			s = (s + rut % 10 * (9 - m++ % 6)) % 11;
		}
		return (dv == (char) (s != 0 ? s + 47 : 75));
	}

	public String clean(String rutDv) {
		rutDv = rutDv.toUpperCase();
		rutDv = rutDv.replace(".", "").replace("-", "").replace(" ", "");
		return rutDv;
	}

	public int getRut(String rutDv) {
		return Integer.parseInt(rutDv.substring(0, rutDv.length() - 1));
	}

	public char getDv(String rutDv) {
		return rutDv.charAt(rutDv.length() - 1);
	}
}
